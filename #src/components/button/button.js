(() => {
    const ButtonsLike = document.querySelectorAll('.button--favorites');

    if (!ButtonsLike) return;

    ButtonsLike.forEach(button => {
        button.addEventListener('click', (e) => {
            e.preventDefault();
            button.classList.toggle('button--favorites-active');
        })
    })
})();