(() => {
    const favoritesTrigger = document.querySelectorAll('.cover__button--favorites');

    favoritesTrigger.forEach(item => {
        item.addEventListener('click', (e) => {
            e.preventDefault();
            item.classList.toggle('cover__button--favorites-active');
        })
    })

})();