/**
 *
 *
 * @author Tatevosyan Artem (@teqst)
 */

(() => {
    const slider = document.querySelector('.detail-item__slider');

    let swiperSlider = null;

    if (!slider) return;

    const initSlider = () => {
        swiperSlider = new Swiper(slider, {
            loop: true,
            navigation: {
                nextEl: '.detail-item__next-slide',
                prevEl: '.detail-item__prev-slide'
            },
            pagination: {
                el: '.detail-item__bullets',
                clickable: true
            }
        });
    };

    document.addEventListener('DOMContentLoaded', initSlider);

})();
