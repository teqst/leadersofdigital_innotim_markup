/**
 *
 *
 * @author Tatevosyan Artem (@teqst)
 */

(() => {
    const allTabs = document.querySelectorAll('.detail-tabs__control-item');
    const allTabsContent = document.querySelectorAll('.detail-tabs__content-item');

    if(!allTabs || !allTabsContent) return;

    allTabs.forEach(tab => {
        tab.addEventListener('click', showTab);
    })

    function showTab(e) {
        allTabs.forEach(i => i.classList.remove('detail-tabs__control-item--active'));
        e.target.classList.toggle('detail-tabs__control-item--active');
        hideTab(e.target.dataset.tabname);
    }

    function hideTab(tabName) {
        allTabsContent.forEach(i => {
            if (tabName === i.dataset.tabname) {
                i.classList.add('detail-tabs__content-item--active');
            } else {
                i.classList.remove('detail-tabs__content-item--active');
            }
        });
    }

})();
