(() => {
    const filters = document.querySelector('.filter__item--filters');
    const filtersWrap = document.querySelector('.filter__item-selectors');

    if(!filters) return;

    filters.addEventListener('click', () => {
        filtersWrap.classList.toggle('filter__item-selectors--active')
    })

})();