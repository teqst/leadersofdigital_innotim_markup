(() => {
    function getPositionAtCenter(element) {
        const {top, left, width, height} = element.getBoundingClientRect();
        return {
            x: left + width / 2,
            y: top + height / 2
        };
    }

    function getDistanceBetweenElements(a, b) {
        const aPosition = getPositionAtCenter(a);
        const bPosition = getPositionAtCenter(b);

        return Math.hypot(aPosition.x - bPosition.x, aPosition.y - bPosition.y);
    }

    const activeSteps = document.querySelectorAll('.timeline__step--active');

    if(!activeSteps.length) return;

    const lastActiveStep = activeSteps[activeSteps.length - 1];
    console.log(lastActiveStep)

    const distance = getDistanceBetweenElements(
        document.querySelector(".timeline__progress"),
        lastActiveStep
    );
    console.log(distance)

    const tip = document.createElement('div');
    tip.className = 'tip';
    tip.textContent = 'Кликните для просмотра';

    lastActiveStep.appendChild(tip);
    lastActiveStep.classList.add('timeline__step--active-blink');


    lastActiveStep.addEventListener('click', () => {
        const infobox = document.querySelector('.infobox');
        infobox.classList.toggle('infobox--active');
    })

    const progress = document.querySelector(".timeline__progress");
    progress.style.width = distance + 'px';

})();