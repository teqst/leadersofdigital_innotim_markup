const gulp = require('gulp'), //Gulp dep.
    browserSync = require('browser-sync').create(), //LiveReload
    pug = require('gulp-pug'), //Pug
    sass = require('gulp-sass')(require('sass')), //SCSS
    sassGlob = require('gulp-sass-glob'), //global imports for sass
    rename = require("gulp-rename"), //gulp-rename provides simple file renaming methods.
    plumber = require('gulp-plumber'), //error display.
    concat = require('gulp-concat'), //This will concat files by your operating systems newLine.
    fileinclude = require('gulp-file-include'), //File include (Modules)
    autoprefixer = require('gulp-autoprefixer'), //Autoprefixer for all browsers
    cleanCSS = require('gulp-clean-css'), //CSS minify
    imagemin = require('gulp-imagemin'), //Minify PNG, JPEG, GIF and SVG images with imagemin
    babel = require('gulp-babel'); //Transpiler for JavaScript


const src = '#src/',
    dist = 'dist/';

const config = {
    src: {
        html: {
            components: src + 'components/**/*.pug',
            pages: src + 'templates/pages/*.pug',
            layouts: src + 'templates/layouts/*.pug',
        },
        style: {
            src: src + 'assets/styles/main.scss',
        },
        js: {
            assetsScripts: src + 'assets/scripts/*.js',
            assetsLibs :src + 'assets/libs/*.js',
            components: src + 'components/**/*.js'
        },
        fonts: src + 'assets/fonts/*.*',
        img: src + 'components/**/assets/*.*',
    },
    dist: {
        html: dist,
        style: dist + 'assets/css/',
        js: dist + 'assets/js/',
        fonts: dist + 'assets/fonts/',
        img: dist + 'assets/img/',
    },
    watch: {
        html: {
            components: src + 'components/**/*.pug',
            templates: src + 'templates/**/*.pug'
        },
        style: {
            assets: src + 'assets/styles/**/*.scss',
            components: src + 'components/**/*.scss'
        },
        js: {
            assets: src + 'assets/scripts/*.js',
            components: src + 'components/**/*.js'
        },
        fonts: src + 'assets/fonts/*.*',
        img: src + 'components/**/assets/*.*',
    }
}

const webServer = () => {
    browserSync.init({
        server: {
            baseDir: dist,
        },
        port: 9000,
        host: 'localhost',
        notify: true,
    })
}

const pugTask = () => {
    return gulp.src([
            config.src.html.pages
        ])
        .pipe(pug({
            pretty: true,
        }))
        .pipe(gulp.dest(config.dist.html))
        .pipe(browserSync.reload({ stream: true }))
}

const scssTask = () => {
    return gulp.src(config.src.style.src)
        .pipe(sassGlob())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(gulp.dest(config.dist.style))
        .pipe(gulp.dest(config.dist.style))
        .pipe(browserSync.reload({ stream: true }))
}

const jsTask = () => {
    let scriptsCollection = [];
    scriptsCollection.push(config.src.js.components);
    scriptsCollection.push(config.src.js.assetsScripts);

    return gulp.src(scriptsCollection)
        .pipe(concat('main.js') )
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(rename( (path) => {
            path.dirname = '';
            return path;
        }))
        .pipe(gulp.dest(config.dist.js))
        .pipe(browserSync.stream());
}

const jsLibs = () => {
    return gulp.src(config.src.js.assetsLibs)
        .pipe(gulp.dest(config.dist.js))
        .pipe(browserSync.stream());
}

const fontsTask = () => {
    return gulp.src(config.src.fonts)
        .pipe(gulp.dest(config.dist.fonts))
        .pipe(browserSync.reload({ stream: true }))
}

const imgTask = () => {
    return gulp.src(config.src.img)
        .pipe(rename( (path) => {
            path.dirname = '';
            return path;
        }))
        .pipe(gulp.dest(config.dist.img))
        .pipe(browserSync.reload({ stream: true }))
}

const watchFiles = () => {
    gulp.watch([config.watch.html.components, config.watch.html.templates], gulp.series(pugTask));
    gulp.watch([config.watch.style.components, config.watch.style.assets], gulp.series(scssTask));
    gulp.watch([config.watch.js.components, config.watch.js.assets], gulp.series(jsTask));
    gulp.watch([config.watch.img], gulp.series(imgTask));
    gulp.watch([config.watch.fonts], gulp.series(fontsTask));
}

//Minify

gulp.task('styles:min', () => {
    return gulp.src(config.src.style.src)
        .pipe(sassGlob())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(gulp.dest(config.dist.style))
        .pipe(cleanCSS({
            level: {
                2: {
                    all: true,
                }
            }
        }))
        .pipe(gulp.dest(config.dist.style))
        .pipe(browserSync.reload({ stream: true }))
})

gulp.task('img:min', () => {
    return gulp.src(config.src.img)
        .pipe(rename( (path) => {
            path.dirname = '';
            return path;
        }))
        .pipe(gulp.dest(config.dist.img))
        .pipe(imagemin())
        .pipe(gulp.dest(config.dist.img))
        .pipe(browserSync.reload({ stream: true }))
})

const all = gulp.series(pugTask, scssTask, jsTask, fontsTask, imgTask, jsLibs)

gulp.task('build', gulp.parallel(all));

exports.default = gulp.parallel(all, watchFiles, webServer);